import Vue from 'vue'
import Vuex from 'vuex'
import Socket from '../sockets/socket'
import { RECEIVED_SOCKET_DATA, SET_TOPIC } from './mutationTypes/socketMutationTypes'

Vue.use(Vuex)

function createWebSocketPlugin (socket) {
  return store => {
    store.subscribe(mutation => {
      if (mutation.type === SET_TOPIC) {
        socket.connect(mutation.payload, (msg) => {
          store.commit(RECEIVED_SOCKET_DATA, msg)
        })
      }
    })
  }
}

export default new Vuex.Store({
  state: {
    data: [],
    topic: ""
  },
  plugins: [createWebSocketPlugin(new Socket())],
  mutations: {
    [RECEIVED_SOCKET_DATA](state, payload){
      state.data = [...payload]
    },
    [SET_TOPIC](state, payload){
      state.topic = payload
    }
  },
  actions: {
    setTopicAction({commit}, topic) {
      commit(SET_TOPIC, topic)
    }
  },
  getters: {
    messages(state){
      return state.data
    }
  },
  modules: {
  },
})
